'''
import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    finally:
        conn.close()

def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)
def main():
    database = "appsqlite.db"

    sql_create_projects_table = """ CREATE TABLE IF NOT EXISTS instances (
                                        id integer PRIMARY KEY,
                                        name_id text NOT NULL,
                                        ip text,
                                        region text
                                    ); """

    sql_create_tasks_table = """CREATE TABLE IF NOT EXISTS gpu (
                                    id integer PRIMARY KEY,
                                    name_id text NOT NULL,
                                    gpu integer,
                                    FOREIGN KEY (instances_id) REFERENCES instances (id)
                                );"""

    # create a database connection
    conn = create_connection(database)
    if conn is not None:
        # create projects table
        create_table(conn, sql_create_projects_table)
        # create tasks table
        create_table(conn, sql_create_tasks_table)
    else:
        print("Error! cannot create the database connection.")

if __name__ == '__main__':
    main()

'''