'''

def instance_info(AWS_REGION):
    client = boto3.client('ec2', region_name=AWS_REGION)
    inst_id = client.describe_instances(
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': [
                    REGEX_NAME,
                ]
            },
            {
                'Name': 'instance-state-name',
                'Values': ['running']
            },
        ],)["Reservations"]
    return inst_id


def add_info(db_name, name_value, ip_addr, reg_name):
    db = sqlite3.connect(db_name)
    cursor = db.cursor()
    cursor.execute("""INSERT INTO instances(name_id, ip, region)
VALUES(?, ?, ?, ?)""", (name_value, ip_addr, reg_name))
    db.commit()

'''