""" fetch init settings"""
from flask import Flask
import logging
from logging.handlers import RotatingFileHandler
from flask_login import LoginManager



app = Flask(__name__)

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'


app.secret_key = 'OWQyMjZjMjEyMDkzMDk3NzMxZGJmZDVh'
from ops_app import views
