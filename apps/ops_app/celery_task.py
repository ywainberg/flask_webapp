from __future__ import absolute_import, unicode_literals
from celery import Celery
#from .settings import CELERY_BROKER


celery_app = Celery('ops_app',
             broker='redis://opsredis:6379/0',
             backend='redis://opsredis:6379/',
             include=['ops_app.tasks'])

# Optional configuration, see the application user guide.
celery_app.conf.update(
    result_expires=3600,
)
#celery_app.conf.broker_url = 'redis://localhost:6379/0'
celery_app.conf.timezone = 'Asia/Jerusalem'

if __name__ == '__main__':
    celery_app.start()