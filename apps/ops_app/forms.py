from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField, TextField, validators, SelectField, PasswordField
from wtforms.validators import DataRequired


class gameForm(FlaskForm):
    name = StringField('Game Id')
    game_id_field = TextField(
        "Game number", [validators.Required("enter game id.")])
    submitgame = SubmitField("Get")


class restartForm(FlaskForm):
    name = StringField('Softlayer Restart')
    restart_id_field = TextField(
        "softlayer id", [validators.Required("enter server sl id.")])
    restart_options = SelectField('restart options', choices=[
                                  ('soft', 'soft reset'), ('soft', ' hard reset ')])
    submitsl = SubmitField("Restart Now")


class LoginForm(FlaskForm):
    """Login form to access writing and settings pages"""
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])


class ChefForm(FlaskForm):
    """chef operation actions form"""
    node_role = SelectField('Choose a role', choices=[
                            ('',''),('gss', 'GSS'), ('gns', 'GNS'), ('*', 'ALL'),('custom','CUSTOM')])
    node_action = SelectField('Select an Action', choices=[
        ('',''),('Add recipe', 'Add recipe'), ('Remove recipe', 'Remove recipe'),('chef-client','run chef-client')])
    node_info = SelectField('Info type', choices=[
        ('',''),('Get current recipes', 'Get current recipes')])
    nodename = StringField('Nodename')
    recipe_name = StringField('Recipe name')
    submmitcf = SubmitField('Submit')
