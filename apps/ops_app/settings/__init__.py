""" define global access variable to resource """

import subprocess
from configparser import SafeConfigParser

parser = SafeConfigParser()

system_name = subprocess.run(['hostname'], shell=True, stdout=subprocess.PIPE)


parser.read('./instance/config.ini')


MONGO_SERVER = parser.get('mongo_conf', 'MONGO_SERVER')
MONGO_USERNAME = parser.get('mongo_conf', 'MONGO_USERNAME')
MONGO_PASSWORD = parser.get('mongo_conf', 'MONGO_PASSWORD')
MONGO_AUTH = parser.get('mongo_conf', 'MONGO_AUTH')
MONGO_SLDB = parser.get('mongo_conf', 'MONGO_SLDB')

SL_USERNAME = parser.get('softlayer_conf', 'SL_USERNAME')
SL_APIKEY = parser.get('softlayer_conf', 'SL_APIKEY')
REDIS_SERVER = parser.get('redis_conf', 'REDIS_URL')
CELERY_BROKER=parser.get('redis_conf', 'CELERY_BROKER_URL')
CELERY_BACKEND=parser.get('redis_conf', 'CELERY_RESULT_BACKEND')
