
import os
import subprocess
import json
from pymongo import MongoClient
from configparser import SafeConfigParser
import time
import datetime
import SoftLayer
from werkzeug.security import generate_password_hash, check_password_hash
import xml.etree.ElementTree as ET
from flask_login import UserMixin
from . import (MONGO_AUTH, MONGO_PASSWORD, MONGO_SERVER, MONGO_USERNAME,
                     MONGO_SLDB, SL_APIKEY, SL_USERNAME)


def mongo_connect(host, user, password, auth_m, source):
    """ connect to mongo db """
    global mongo
    mongo = MongoClient(host, username=user, password=password,
                        authMechanism=auth_m, authSource=source)


def db_access(db_name, collection_name):
    """ connect to db collection """
    db = mongo[db_name]
    collection_name = db[collection_name]
    return collection_name


def current_time():
    current_date = str(datetime.datetime.now())
    return datetime.datetime.strptime(current_date, '%Y-%m-%d %H:%M:%S.%f').strftime('%Y-%m-%d %H:%M:%S')


def softlayer_connect(user, password):
    """ connect to softlayer """
    global client
    client = SoftLayer.Client(username=user,
                              api_key=password, timeout=0)


def reboot_server(hw_id, action):
    """Restrat Server options """
    global srv
    srv = client['SoftLayer_Hardware_Server']
    if action == 'soft':
        srv.rebootSoft(id=hw_id)
    elif action == 'hard':
        srv.rebootHard(id=hw_id)
    else:
        pass


def reload_os(hw_id):
    """Reload the windows OS"""
    srv.reloadCurrentOperatingSystemConfiguration(id=hw_id)


softlayer_connect(SL_USERNAME, SL_APIKEY)
mongo_connect(MONGO_SERVER, MONGO_USERNAME, MONGO_PASSWORD,
              MONGO_AUTH, MONGO_SLDB)

portal_collection = db_access(MONGO_SLDB, 'sl_portal')
aws_collection = db_access(MONGO_SLDB, 'aws')
regions_collection = db_access(MONGO_SLDB, 'aws_regions')
aws_instance_collection = db_access(MONGO_SLDB, 'aws_instance')
spec_collection = db_access(MONGO_SLDB, 'sl_spec')
gss_collection = db_access(MONGO_SLDB, 'gss_app')
area_list_collection = db_access(MONGO_SLDB, 'area_list')
game_list_collection = db_access(MONGO_SLDB, 'game_list')
user_list_collection = db_access(MONGO_SLDB, 'Users')


def actions(id_key):
    return '%s restart server button' % id_key


def restart_action(s_id):
    return actions(s_id)

def game_id(id_number):
    if id_number in game_list_collection.find({}).distinct('Id'):
        return game_list_collection.find({'Id': id_number}).distinct('Name')
    else:
        return f"no match found for {id_number}"
class BaseGss():
    def __init__(self, doc_id):
        self.doc_id = doc_id

    """class for base setting of gss"""

    def gss_id(self):
        return gss_collection.find({'_id': self.doc_id}).distinct('id')

    def area_id(self):
        return gss_collection.find({'_id': self.doc_id}).distinct('areaId')

    def gss_ip(self, doc_id):
        return gss_collection.find({'_id': self.doc_id}).distinct('ip')


class ExtraGss():
    def __init__(self, doc_id):
        self.doc_id = doc_id
    
    def gss_version(self, param):
        body=gss_collection.find({'_id': self.doc_id}).distinct('Xml')
        tree = ET.ElementTree(ET.fromstring(body))
        root = tree.getroot()
        for Name, Value in root.items():
            if Name == param:
                return Value
    
class SoftLayerGss(BaseGss):

    # def status_list(self, s_id):
        # return gss_collection.find({'_id': s_id}).distinct('hardwareStatus.status')

    def server_list(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('operatingSystem.hardwareId')

    def dc_list(self, s_id):
        if gss_collection.find({'_id': s_id}).distinct('datacenter.longName'):
            return gss_collection.find({'_id': s_id}).distinct('datacenter.longName')
        else:
            return gss_collection.find({'_id': s_id}).distinct('datacenter_name')

    def public_list(self, s_id):
        if gss_collection.find({'_id': s_id}).distinct('primaryIpAddress'):
            return gss_collection.find({'_id': s_id}).distinct('primaryIpAddress')
        else:
            return gss_collection.find({'_id': s_id}).distinct('ip')

    def public_gateway(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('networkComponents.2.primarySubnet.gateway')

    def public_netmask(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('networkComponents.2.primarySubnet.netmask')

    def private_list(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('primaryBackendIpAddress')

    def private_gateway(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('networkComponents.1.primarySubnet.gateway')

    def private_netmask(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('networkComponents.1.primarySubnet.netmask')

    def created_list(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('provisionDate')

    def ipmi_cred(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('remoteManagementAccounts.0.password')

    def ipmi_list(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('networkManagementIpAddress')

    def bandwidth_allocation(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('BandwidthAllocation')

    def open_ticket_list(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('OpenTickets')

    def vpn_url_site(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('vpn_url')

    def server_cost(self, s_id):
        return gss_collection.find({'_id': s_id}).distinct('cost')

    def disk_size(self, s_id):
        for element in gss_collection.find({'_id': s_id}).distinct('components'):
            if element['hardwareComponentModel']['hardwareGenericComponentModel']['hardwareComponentType']['keyName'] == 'HARD_DRIVE':
                yield element['hardwareComponentModel']['hardwareGenericComponentModel']['capacity']

    def gpu_type(self, s_id):
        for card in gss_collection.find({'_id': s_id}).distinct('components'):
            if card['hardwareComponentModel']['manufacturer'] == 'NVIDIA':
                yield card['hardwareComponentModel']['name']

    def cpu_type(self, s_id):
        for ip in spec_collection.find({}):
            if gss_collection.find({'_id': s_id}).distinct('ip')[0] == ip['ip']:
                return ip['cpu type']

    def motherboard(self, s_id):
        for ip in spec_collection.find({}):
            if gss_collection.find({'_id': s_id}).distinct('ip')[0] == ip['ip']:
                return ip['motherboard']


class Ami():

    def region_name(self, ami_id):
        reg_name = aws_collection.find(
            {'ami_info.ami_id': ami_id}).distinct('ami_info.region')
        return regions_collection.find({'RegionName': reg_name[0]}).distinct('Alias')

    def ami_name(self, ami_id):
        return aws_collection.find({'ami_info.ami_id': ami_id}).distinct('ami_info.ami_name')

    def ami_id(self, ami_id):
        return aws_collection.find({'ami_info.ami_id': ami_id}).distinct('ami_info.ami_id')

    def ami_date(self, ami_id):
        return aws_collection.find({'ami_info.ami_id': ami_id}).distinct('ami_info.ami_created')

    def ebs(self, ami_id):
        for e in aws_collection.find({'ami_info.ami_id': ami_id}).distinct('ami_info.ebs'):
            yield str(e['Ebs']['VolumeSize'])

    def ebs_count(self, ami_id):
        count = 0
        for e in aws_collection.find({'ami_info.ami_id': ami_id}).distinct('ami_info.ebs'):
            count += 1
        return count


class Instance():

    def instance_region(self, ins_id):
        return aws_instance_collection.find({"Instances.InstanceId": ins_id}).distinct("Instances.Placement.AvailabilityZone")[0]

    def instance_name(self, ins_id):
        if aws_instance_collection.find({"Instances.InstanceId": ins_id}).distinct("Instances.Tags.Value"):
            return aws_instance_collection.find({"Instances.InstanceId": ins_id}).distinct("Instances.Tags.Value")[0]
        else:
            return " "

    def instance_status(self, ins_id):
        return aws_instance_collection.find({"Instances.InstanceId": ins_id}).distinct("Instances.State.Name")[0]

    def instance_lunchtime(self, ins_id):
        FMT = '%Y-%m-%d %H:%M:%S'
        timestamp = aws_instance_collection.find(
            {"Instances.InstanceId": ins_id}).distinct("Instances.LaunchTime")[0]
        time_sec = datetime.datetime.strptime(
            current_time(), FMT) - datetime.datetime.strptime(str(timestamp), FMT)
        return "Days = %s , Hours = %s" % (round(time_sec.days), round(time_sec.seconds // 3600))

    def instance_ip(self, ins_id):
        return aws_instance_collection.find({"Instances.InstanceId": ins_id}).distinct("Instances.PublicIpAddress")

    def instance_ami(self, ins_id):
        return aws_instance_collection.find({"Instances.InstanceId": ins_id}).distinct("Instances.ImageId")[0]

class User():
    
    def __init__(self, username):
        self.username = username

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.username

    @staticmethod
    def validate_login(password_hash, password):
        return check_password_hash(password_hash, password)





def node_list(name):
    """ Get node list array"""
    os.chdir('/home/yaron/Dropbox/chef-repo')
    if '-' in name:
        choice_list = subprocess.run([f'knife search node "name:{name}" |grep "Name" | cut -d ":" -f2 |tr -s [:space:]'], shell=True, stdout=subprocess.PIPE)
        return [choice_list.stdout.decode("utf-8").split("\n")[0].strip()]
    elif name == 'gss' or 'gns' or '*':
        choice_list = subprocess.run(
            [f'knife search node "role:{name}" |grep "Name" | cut -d ":" -f2 |tr -s [:space:]'], shell=True, stdout=subprocess.PIPE)
        return choice_list.stdout.decode("utf-8").split("\n")





def push_command(command_name, role_name):
    """ pushing a command to node """
    return subprocess.run(
        [f"knife job start --quorum 90% {command_name} --search 'role:{role_name}'"],
        shell=True, stdout=subprocess.PIPE)

def get_action(userinput,name, *args):
    """ your actions are in progress """
    os.chdir('/home/yaron/Dropbox/chef-repo')
    if args:
        for arg in args:
            arg1 = arg
    if userinput == 'Add recipe':
        for node in node_list(name):
            if node != "":
                subprocess.run(
                    [f"knife node run_list add {node} {arg1}"], shell=True, stdout=subprocess.PIPE)
    elif userinput == 'Remove recipe':
        for node in node_list(name):
            if node != "":
                subprocess.run(
                    [f"knife node run_list remove {node} {arg1}"], shell=True, stdout=subprocess.PIPE)
    elif userinput == 'run chef-client':
        for node in node_list(name):
            if node != "":
                push_command('chef-client', node)

def get_node_info(user_input,node_name):
    if user_input == 'Get current recipes':
        for d in node_list(node_name):
            if d != "":
                run_list = subprocess.run([f"knife node show {d} --format json"], shell=True, stdout=subprocess.PIPE)
                yield json.loads(run_list.stdout.decode("utf-8"))
                
