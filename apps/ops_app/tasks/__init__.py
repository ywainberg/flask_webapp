
""" setting path to root folder"""

import subprocess
import sys
system_name = subprocess.run(['hostname'], shell=True, stdout=subprocess.PIPE)
path = "/home/yaron/Dropbox/flask_webapp/apps"
if system_name.stdout.decode('utf-8') == 'ops.playcast-media.com\n':
    path = "/var/www/flask_webapp/apps"
if path not in sys.path:
    sys.path.append(path)
