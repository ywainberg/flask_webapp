"""
    Check running instance report
"""
from __future__ import absolute_import, unicode_literals
import subprocess
import boto3
from pymongo import MongoClient
from ops_app.settings import (MONGO_AUTH, MONGO_PASSWORD, MONGO_SERVER,
                              MONGO_USERNAME, MONGO_SLDB)
from ops_app.celery_task import celery_app


def mongo_connect(host, user, password, auth_m, source):
    """ connect to mongo db """
    global mongo
    mongo = MongoClient(host, username=user, password=password,
                        authMechanism=auth_m, authSource=source)


def db_access(db_name, collection_name):
    """ connect to db collection """
    db = mongo[db_name]
    collection_name = db[collection_name]
    return collection_name


def aws_regions():
    """
        List all AWS ZONES
    """
    ec3 = boto3.client('ec2')
    return [region['RegionName']for region in ec3.describe_regions()['Regions']]


mongo_connect(MONGO_SERVER, MONGO_USERNAME, MONGO_PASSWORD,
              MONGO_AUTH, MONGO_SLDB)

aws_instance_collection = db_access(MONGO_SLDB, 'aws_instance')

@celery_app.task
def ec2_instance():
    # clean collection before new data
    aws_instance_collection.delete_many({})

    for a in aws_regions():
        client = boto3.client('ec2', region_name=a)
        for i in client.describe_instances()['Reservations']:
            try:
                aws_instance_collection.insert_one(i)

            except TypeError as typ:
                return "Type Error {}".format(typ)
            except ValueError as val:
                return "Value Error {}".format(val)
                
#subprocess.run(['systemctl restart httpd'], shell=True, stdout=subprocess.PIPE)


if __name__ == '__main__':
    pass
