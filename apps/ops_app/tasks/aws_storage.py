""" Retrive EBS storage information"""
from __future__ import absolute_import, unicode_literals
import subprocess
import boto3
from pymongo import MongoClient
from ops_app.celery_task import celery_app
from ops_app.settings import (MONGO_AUTH, MONGO_PASSWORD, MONGO_SERVER,
                     MONGO_USERNAME, MONGO_SLDB)


def aws_regions():
    """
        List all AWS ZONES
    """
    ec3 = boto3.client('ec2')
    return [region['RegionName']for region in ec3.describe_regions()['Regions']]


def image_list():
    for reg in aws_regions():
        ec2 = boto3.resource('ec2', region_name=reg)
        for ami in ec2.images.filter(Owners=['801651538512']):
            yield [reg, ami.name, ami.image_id, ami.creation_date, ami.block_device_mappings]


def mongo_connect(host, user, password, auth_m, source):
    """ connect to mongo db """
    global mongo
    mongo = MongoClient(host, username=user, password=password,
                        authMechanism=auth_m, authSource=source)


def db_access(db_name, collection_name):
    """ connect to db collection """
    db = mongo[db_name]
    collection_name = db[collection_name]
    return collection_name


mongo_connect(MONGO_SERVER, MONGO_USERNAME, MONGO_PASSWORD,
              MONGO_AUTH, MONGO_SLDB)

aws_collection = db_access(MONGO_SLDB, 'aws')

regions_collection = db_access(MONGO_SLDB, 'aws_regions')



##########################################################################
#### Insert all device info to database ##################################
@celery_app.task
def ami_task():
    aws_collection.delete_many({})
    if aws_collection.count() == 0:
        try:
            for img in image_list():
                aws_collection.insert_one(

                    {"ami_info": {

                        "region": img[0],
                        "ami_name": img[1],
                        "ami_id": img[2],
                        "ami_created": img[3],
                        "ebs": img[4]

                    }
                    })

        except TypeError as object_type:
            return "the error is related to type of the object\n %s" % object_type

#subprocess.run(['systemctl restart httpd'], shell=True, stdout=subprocess.PIPE)
