#!/usr/bin/python
import os
import subprocess
import logging

# check parsing
# knife search node 'name:*' |grep 'Name' | cut -d ":" -f2 |tr -s [:space:]


def print_progress(func):
    def wrapper(*args, **kwargs):
        return func.__doc__
    return wrapper


def show_result(node, action):
    print(f'{node} : {action} operation is successfully finish')


try:
    roles = int(input(
        "please type the relevant number for the search:\n1: GSS\n2: GNS\n3: ALL\n4:CUSTOM\n\n"))
    role = {
        1: "gss",
        2: "gns",
        3: "*",
    }
    if roles == 4:
        custom_node = input("type node name\n").upper()
        choice_list = subprocess.run(
            [f'knife search node "name:{custom_node}" |grep "Name" | cut -d ":" -f2 |tr -s [:space:]'], shell=True, stdout=subprocess.PIPE)
        parsed_list = [choice_list.stdout.decode(
            "utf-8").split("\n")[0].strip()]
    else:
        choice_list = subprocess.run(
            [f'knife search node "role:{role[roles]}" |grep "Name" | cut -d ":" -f2 |tr -s [:space:]'], shell=True, stdout=subprocess.PIPE)
        parsed_list = choice_list.stdout.decode("utf-8").split("\n")

except ValueError:
    print("you  didn't type a number between 1-3")


def push_command(command_name, role_name):
    """ pushing a command to node """
    return subprocess.run(
        [f"knife job start --quorum 90% {command_name} --search 'role:{role_name}'"],
        shell=True, stdout=subprocess.PIPE)


def get_action():
    """ your actions are in progress """
    actions = int(input(
        "please select the action:\n1:add recipe\n2:remove recipe\n3:get current recipes\n4:run chef-client\n\n"))
    if actions == 1:
        add_recipe = input("type the recipe you want to add:\n")
        for node in parsed_list:
            if node != "":
                subprocess.run(
                    [f"knife node run_list add {node} '{add_recipe}'"], shell=True, stdout=subprocess.PIPE)
                show_result(node, 'add recipe')
    elif actions == 2:
        remove_recipe = input("type the recipe you want to remove:\n")
        for node in parsed_list:
            if node != "":
                subprocess.run(
                    [f"knife node run_list remove {node} '{remove_recipe}'"], shell=True, stdout=subprocess.PIPE)
                show_result(node, 'remove recipe')
    elif actions == 3:
        for node in parsed_list:
            if node != "":
                run_list = subprocess.run(
                    [f"knife node show {node} |grep 'Run List'"], shell=True, stdout=subprocess.PIPE)
                print(f'{node} : {run_list.stdout.decode("utf-8")}')
    else:
        for node in parsed_list:
            if node != "":
                push_command('chef-client', node)
                show_result(node, 'run chef-client')


get_action()
