""" backend for softlayer web app """
from __future__ import absolute_import, unicode_literals
import re
import subprocess
import SoftLayer
import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient
from ops_app.settings.classes import (mongo_connect, db_access,mongo,softlayer_connect,client,reboot_server,reload_os)
from ops_app.settings import (MONGO_AUTH, MONGO_PASSWORD, MONGO_SERVER, MONGO_USERNAME,MONGO_SLDB, SL_APIKEY, SL_USERNAME)
from ops_app.celery_task import celery_app
import json

# Function Definition

def id_list():
    """ list all hardware id in my account """
    global mgr
    mgr = SoftLayer.HardwareManager(client)
    return mgr.list_hardware(mask='id')



def hardware_status(hw_id):
    """ get device status """
    return mgr.get_hardware(hardware_id=hw_id)['hardwareStatus']['status']


def all_hardware(hw_id):
    """ get all hardware info """
    return mgr.get_hardware(hardware_id=hw_id)


def components(hw_id):
    """ get hardware components """
    return client.call('Hardware', 'getComponents', id=hw_id)


def server_cost(hw_id):
    """ get server cost in $dollar"""
    service = client['SoftLayer_Hardware_Server']
    return service.getCost(id=hw_id)


def dc_names(hw_id):
    """ fetch datacenter short name """
    return gss_collection.find_one({'id': hw_id})['datacenter']['name']


def vpn_site(url, dc_name):
    """ list vpn site per datacenter """
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    global vpn
    vpn = soup.find_all(href=re.compile('vpn.'))
    for vp in vpn:
        if dc_name in vp.get('href'):
            return vp.get('href')


def document_value_list(collection_name, search_key):
    """ Get id value from device_id collection """
    ids_list = []
    for value in collection_name.distinct(search_key):
        ids_list.append(value)
    return ids_list


def bandwidth(user, password, number_id):
    """ get bandwidth for each server """
    band = requests.get(
        'https://%s:%s@api.softlayer.com/rest/v3/SoftLayer_Hardware/%s/BandwidthAllocation' % (user, password, number_id))
    return band.json()


def open_ticket(user, password, number_id):
    """ fetch tickets per server """
    tickets = requests.get(
        'https://%s:%s@api.softlayer.com/rest/v3/SoftLayer_Hardware_Server/%s/getActiveTickets' % (user, password, number_id))
    for ticket in tickets.json():
        if 'title' in ticket:
            return ticket['title']
        else:
            return "No Open Ticket"


def find_gss(ipaddress):
    """ return all gss list from gnsott api"""
    gsslist = requests.get(
        'https://ottgw.playcast-media.com:443/api/Streaming/GetGssList')
    for i in gsslist.json():
        if ipaddress in i['ip']:
            return i

def gss_xml(ipaddress):
    body=requests.get('https://ottgw.playcast-media.com:443/api/Streaming/GetGssList')
    for a in body.json():
        if ipaddress in a['ip']:
            return a
# Access Definition


mongo_connect(MONGO_SERVER, MONGO_USERNAME, MONGO_PASSWORD,
              MONGO_AUTH, MONGO_SLDB)

softlayer_connect(SL_USERNAME, SL_APIKEY)

id_list()


deviceid_collection = db_access(MONGO_SLDB, 'device_id')

gss_collection = db_access(MONGO_SLDB, 'gss_app')

gss_list_collection = db_access(MONGO_SLDB, 'gss_list')

area_list_collection = db_access(MONGO_SLDB, 'area_list')

game_list_collection = db_access(MONGO_SLDB, 'game_list')



# clean softlayer instance
@celery_app.task
def clean_instance():
    deviceid_collection.delete_many({})
    for device in id_list():
        deviceid_collection.insert_one(device)


    if not deviceid_collection.count() == 0:
        try:
            device_list = {}
            for device in id_list():
                device_list.update(device)
                id_number = deviceid_collection.find_one(device)['id']
                if hardware_status(id_number) != 'ACTIVE':
                    deviceid_collection.delete_one(device)
            for s in deviceid_collection.find({}):
                if not any(s for s in device_list):
                    deviceid_collection.delete_one(s)

# clean gamefly instance
            for p in gss_collection.distinct('ip'):
                if not gss_list_collection.find_one({'ip': p}):
                    gss_collection.delete_one({'ip': p})

        except ValueError as val:
            print("Value Error: {}".format(val))
        except TypeError as typ:
            print("Type Error: {}".format(typ))
        else:
            pass

# clean current collection info
@celery_app.task
def extra_collection():
    area_list_collection.delete_many({})
    gss_list_collection.delete_many({})
    game_list_collection.delete_many({})

# Preapre extra collections

    areaList = requests.get(
        'https://ottgw.playcast-media.com:443/api/Streaming/GetAreas')
    for area in areaList.json():
        area_list_collection.insert_one(area)

    gamelist = requests.get(
        'https://ottgw.playcast-media.com:443/api/Games/GetAvailable')
    for game in gamelist.json():
        game_list_collection.insert_one(game)

    glist = requests.get(
        'https://ottgw.playcast-media.com:443/api/Streaming/GetGssList')
    for g in glist.json():
        gss_list_collection.insert_one(g)

##########################################################################
#### Insert all info to database ##################################
@celery_app.task
def update_gss_db():
    
    if not gss_collection.count() == 0:
        try:
            for s in document_value_list(deviceid_collection, 'id'):
                for ip in gss_list_collection.find({}).distinct('ip'):
                    if ip == all_hardware(s)['primaryIpAddress']:
                        gss_collection.update_one(
                            {"ip": ip}, {"$set": all_hardware(s)}, upsert=True)
                        gss_collection.update_one({"ip": ip}, {"$set": {"BandwidthAllocation": bandwidth(
                            SL_USERNAME, SL_APIKEY, s)}}, upsert=True)
                        gss_collection.update_one({"ip": ip}, {"$set": {"OpenTickets": open_ticket(
                            SL_USERNAME, SL_APIKEY, s)}}, upsert=True)
                        gss_collection.update_one(
                            {"ip": ip}, {"$set": {"components": components(s)}}, upsert=True)
                        gss_collection.update_one({"ip": ip}, {"$set": {"vpn_url": vpn_site(
                            'https://www.softlayer.com/VPN-Access', dc_names(s))}}, upsert=True)
                        gss_collection.update_one(
                            {"ip": ip}, {"$set": {"cost": server_cost(s)}}, upsert=True)

            for ip in gss_collection.find({}).distinct('ip'):
                for ip2 in gss_list_collection.find({}):
                    if ip == ip2['ip']:
                        gss_collection.update_one(
                            {"ip": ip}, {"$set": {"id": ip2['id']}}, upsert=True)
                        gss_collection.update_one(
                            {"ip": ip}, {"$set": {"xml": gss_xml(ip)}}, upsert=True)

            for area_id in gss_collection.find({}).distinct('areaId'):
                getareaid = area_list_collection.find_one({"id": area_id})
                gss_collection.update_many(
                    {"areaId": area_id}, {"$set": {"datacenter_name": getareaid['name']}}, upsert=True)


        except TypeError as object_type:
            print("the error is related to type of the object\n %s" % object_type)
    else:
        for ip in gss_list_collection.find({}).distinct('ip'):
            gss_collection.insert_one(find_gss(ip))

