""" web app for softlayer hardware info """
import os
from ops_app import logging
from ops_app import RotatingFileHandler
import requests
import flask
from flask import render_template, request ,flash ,redirect ,url_for ,jsonify
from flask_login import login_user, logout_user, login_required
from .settings import classes
from .settings.classes import User ,user_list_collection ,node_list, push_command ,get_action ,get_node_info
from . import app ,lm
from .forms import gameForm, restartForm, LoginForm ,ChefForm
from ops_app.settings import REDIS_SERVER
from ops_app.tasks.aws_storage import ami_task
from ops_app.tasks.aws_instances import ec2_instance
from ops_app.tasks.softlayer_backend import clean_instance
from ops_app.tasks.softlayer_backend import extra_collection
from ops_app.tasks.softlayer_backend import update_gss_db
from .celery_task import celery_app

#######################################################
########  Function Definition #########################



def actions(id_key):
    return '%s restart server button' % id_key

# set logging level
logging.basicConfig(level=logging.INFO)

ec2_instance.apply_async(countdown=3600)
ami_task.apply_async(countdown=18000)
clean_instance.apply_async(countdown=86400)
update_gss_db.apply_async(countdown=600)
extra_collection.apply_async(countdown=86400)



@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'POST' and form.validate_on_submit():
        user = user_list_collection.find_one({"_id": form.username.data})
        if user and User.validate_login(user['password'], form.password.data):
            user_obj = User(user['_id'])
            login_user(user_obj)
            app.logger.info('User has logged in successfully')
            flash("Logged in successfully!", category='success')
            return redirect(request.args.get("next") or url_for("main_view"))
        app.logger.error('Failed login')
        flash("Wrong username or password!", category='error')
    return render_template('login.html', title='login', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))




@app.route('/', methods=['GET', 'POST'])
@login_required
def main_view(): 
    """primary site function """
    class_sl = []
    temp_list = classes.gss_collection.find({}).distinct('_id')
    for obj in temp_list:
        softlayer_info = classes.SoftLayerGss(obj)
        class_sl.append([
            softlayer_info.gss_id(), softlayer_info.server_cost(obj), softlayer_info.dc_list(
                obj), softlayer_info.public_list(obj), softlayer_info.private_list(obj), softlayer_info.created_list(
                obj), softlayer_info.bandwidth_allocation(obj), list(softlayer_info.disk_size(obj)), softlayer_info.ipmi_list(
                    obj), softlayer_info.vpn_url_site(obj), list(softlayer_info.gpu_type(obj)), softlayer_info.open_ticket_list(
                        obj), softlayer_info.server_list(obj), softlayer_info.private_gateway(obj), softlayer_info.private_netmask(
                            obj), softlayer_info.public_gateway(obj), softlayer_info.public_netmask(obj), softlayer_info.ipmi_cred(
                                obj), softlayer_info.cpu_type(obj), softlayer_info.motherboard(obj), softlayer_info.area_id()
        ])
    gform = gameForm()
    rform = restartForm()
    if request.method == 'POST':
        if gform.submitgame.data and gform.validate():
            game = int(gform.game_id_field.data)
            game_id =  classes.game_id(game)[0]
            return render_template('index.html', mongo_list=temp_list, class_sl=class_sl, gform=gform,result=game_id, rform=rform)
        if rform.submitsl.data and rform.validate():
            classes.reboot_server(rform.restart_id_field.data, rform.restart_options.data)
            return render_template('index.html', mongo_list=temp_list, class_sl=class_sl, gform=gform,rform=rform)
            

    return render_template('index.html', mongo_list=temp_list, class_sl=class_sl,gform=gform,rform=rform)


app.logger.info('render softlayer page')


def get_color(value):
    return "green" if value > 0 else "red"

@app.route('/chef', methods=['GET','POST'])
def chef_action():
    """ chef server action forms"""
    cform = ChefForm()
    if request.method == 'POST' and cform.validate():
        role = cform.node_role.data
        cinfo = cform.node_info.data
        if 'Get current recipes' in cinfo:
            recipe_info=get_node_info(cinfo,role)
            #recipe_info.append(get_node_info(cinfo,nd))
            #print(recipe_info)
            return render_template('chef.html',cform=cform,recipe_info=recipe_info)
        if role == 'custom':
            role = cform.nodename.data.upper()
            app.logger.info(role)
        action = cform.node_action.data
        app.logger.info(action)
        if 'Remove' or 'Add' in action:
            get_action(action,role,cform.recipe_name.data)
            flash('Task run successfully')
            return render_template('chef.html',cform=cform)
        return render_template('chef.html',cform=cform)
    return render_template('chef.html',cform=cform)   
            


@app.route('/games/api/v1.0/games/<int:game_ids>', methods=['GET'])
def get_games(game_ids):
    return jsonify({"all_games" : classes.game_id(game_ids)[0]})

@app.route('/reload', methods=['GET', 'POST'])
def reload():
    select = request.form.get('reload')
    server_id = str(select)
    print(server_id)
    # reload_os(server_id)
    return render_template('index.html')


@app.route('/aws', methods=['GET', 'POST'])
@login_required
def aws_storage():
    amis = []
    ami_list = classes.aws_collection.find({}).distinct('ami_info.ami_id')
    for am in ami_list:
        ami_data = classes.Ami()
        amis.append([ami_data.region_name(am), ami_data.ami_name(am), ami_data.ami_id(
            am), ami_data.ami_date(am), list(ami_data.ebs(am)), ami_data.ebs_count(am)])

    ins = []
    ins_list = classes.aws_instance_collection.find(
        {}).distinct("Instances.InstanceId")
    for server in ins_list:
        server_data = classes.Instance()
        ins.append([server_data.instance_region(server), server_data.instance_name(server), server_data.instance_status(
            server), server_data.instance_lunchtime(server), server_data.instance_ip(server), server_data.instance_ami(server)])
    if request.method == "POST":
        if request.form.get('ami'):
            ami_task.delay()
            app.logger.info('user refresh ami data')
        elif request.form.get('ec2'):
            ec2_instance.delay()
            app.logger.info('user refresh instance data')
    return render_template('aws_storage.html', images=amis, instance_list=ins)


app.logger.info('render aws page')


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

@lm.user_loader
def load_user(username):
    u = user_list_collection.find_one({"_id": username})
    if not u:
        return None
    return User(u['_id'])