
from flask_wtf import Form
from wtforms import StringField, BooleanField, SubmitField, TextField, validators, SelectField, PasswordField
from wtforms.validators import DataRequired


class EncryptionForm(Form):
    """encrypt gpg From"""
    password = PasswordField('Password')
    gpg_format = PasswordField('gpg_format')


#class DecryptionForm(Form):
#    """decrypt gpg From"""
    
