
class Search{
    // 1.describe and create/initiate out object
    constructor(url) {
        this.api = url;
        this.searchField = $("#game_id_field");
        this.submit = $("#subgame");
        this.events();
    }

    // 2.events
    events(){
        this.submit.on("click", this.getResults.bind(this));
    }

    // 3.method (functions, actions...)


    getResults(){
        let id;
        console.log("processing call to "+this.api);
        $.getJSON(this.api+this.searchField.val(),game => {
                if (game.all_games!="n"){ 
                    this.renderResult(game.all_games);
                }else{
                    this.renderResult("Game not found");
                }
        });
    }
    
    renderResult(item){
        if ( $("div.item_game").length == 0){
            $("#games").append(`
                <div class="container item_game">
                    <div id="search-overlay__results">
                        <p>${item}</p>
                    </div>
                </div>
            
            `);
        } else {
            $("div.item_game").replaceWith(`
                <div class="container item_game">
                    <div id="search-overlay__results">
                        <p>${item}</p>
                    </div>
                </div>
            `);
        }
    }
    
}
