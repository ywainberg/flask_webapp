
import gnupg
#import random
import datetime
import os



name = "EACG Ops"
email = "il-ops@ea.com"
comment = "cloud gaming password app"
gpg = gnupg.GPG(homedir=os.environ['GPGHOME'], verbose=True,keyring='pubring.gpg',secring='secring.gpg')
gpg.encoding = 'utf-8'
recipient="il-ops@ea.com"




#def encrypt_data(clear_passowrd):
#    encrypted_ascii_data = str(gpg.encrypt(clear_passowrd,fingerprint))
#    return f'"""{encrypted_ascii_data}"""'


#def decrypt_data(gpg_message):
#    message= re.sub("----- ", "-----\n", gpg_message)
#    return str(gpg.decrypt(message))


def encrypt_action(text):
    formating=datetime.datetime.now().isoformat().replace('-','')[::-1]
    filename=''.join(e for e in formating if e.isalnum())
    encrypted_data = gpg.encrypt(text,'2854E95A926F063A3AFB42D1B6BB42F51D7B2883')
    f =open(filename,"w+")
    f.write(str(encrypted_data))
    f.close()
    return filename

def decrypt_action(text):
    try:
        f =open(text.strip(),"rb")
        decrypted_data = gpg.decrypt_file(f)
        f.close()
        return str(decrypted_data)
    except FileNotFoundError:
        return 'Not a Valid Secure String'
    except:
        return 'An Error, Contact Yaron'
