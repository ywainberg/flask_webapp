
from flask import Flask
import logging
from logging.handlers import RotatingFileHandler
from flask_login import LoginManager
from forms import EncryptionForm
import requests
from flask import render_template, request ,flash ,redirect ,url_for ,jsonify
from flask_login import login_user, logout_user, login_required
from tasks import encrypt_action,decrypt_action
from werkzeug import serving
import ssl


app = Flask(__name__)

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'


app.secret_key = 'OWQyMjZjMjEyMDkzMDk3NzMxZGJmZDVh'



@app.route('/', methods=['GET', 'POST'])
def login():
    form = EncryptionForm()
    if form.validate_on_submit() and request.method == 'POST':
        if request.form['action'] == 'Get Password':
            app.logger.info('hash to pass action')
            data = decrypt_action(form.gpg_format.data)
            return render_template('login.html', title='password', form=form,data=data)
        elif request.form['action'] == 'Get Hash':
            app.logger.info('password convert action')
            data= str(encrypt_action(form.gpg_format.data))
            return render_template('login.html', title='password', form=form, data=data)
        return redirect(url_for('login'))
    return render_template('login.html', title='password', form=form)


if __name__ == '__main__':
    handler = RotatingFileHandler('ops_app.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.load_cert_chain("./_.cg.ea.com.crt", "./_.cg.ea.com.key",password=decrypt_action('sphrase'))
    app.run(host='0.0.0.0', debug=True, port=443,ssl_context=context)