from ops_app import app, RotatingFileHandler, logging


if __name__ == '__main__':
    handler = RotatingFileHandler('ops_app.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    app.run(host='0.0.0.0', debug=True, port=5000)