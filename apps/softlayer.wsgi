import os
import logging
import sys
#import site

logging.basicConfig(stream=sys.stderr)

# Add virtualenv site packages
#site.addsitedir(os.path.join(os.path.dirname(__file__), 'venv/lib/python3.6/site-packages'))

# Path of execution
sys.path.append('/var/www/flask_webapp/apps')

# Fired up virtualenv before include application
#activate_env = os.path.expanduser(os.path.join(os.path.dirname(__file__), 'venv/bin/activate_this.py'))
##execfile(activate_env, dict(__file__=activate_env))

# import my_flask_app as application
from ops_app import app as application
